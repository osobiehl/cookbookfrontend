


/*
    Rendering of stuff related to ingredients
//constructs each row of a table of ingredients
props:
ingredient: an individual ingredient object to be rendered

 */


import React from "react";
const Config = require('./config.json');
const DomainLink = Config.host + Config.port + Config.restApiRoot;

class IngredientsRow extends React.Component{
    constructor(props) {
        super(props);
        this.onRowClick = this.onRowClick.bind(this);
        this.onDeleteIngredient = this.onDeleteIngredient.bind(this);
        this.ing = this.props.ingredient;
        this.state = {
            deleted: false
        }

    }
    onRowClick(ing){
        this.props.handleRowClick(ing);
    }
    onDeleteIngredient(e){
        this.props.onRowDeselect(e);
        this.setState({
            deleted:true
        });
        var mes = new XMLHttpRequest();
        mes.open('DELETE', DomainLink + Config.DeleteIngredient+this.ing.idRecipeIngredientRelation)
        mes.send();
        mes.onload = function(){
            console.log("recipe ingredient deleted successfully");
        }
    }
    render() {
        let DeleteButton = <button onClick={this.onDeleteIngredient}> X </button>
        if (!this.state.deleted)
            return (
                <tr onClick={() => this.onRowClick(this.ing)}>
                    <td>{this.ing.ingredient}</td>
                    <td>{this.ing.amount}</td>
                    <td>{this.ing.description}</td>
                    <td>{DeleteButton}</td>
                </tr>
            )
        else return(null);
    }
}
/*
    Creates a table of ingredients, responsible for:
    handling if an ingredient wants to be added
    an existing ingredient wants to be edited
    an existing ingredient wants to be deleted
    props:
    recipeSteps: an array of ingredients objects, containing their description, amounts, and names


 */

class IngredientsTable extends React.Component{
    constructor(props) {
        super(props);
        this.handleAddIngredient = this.handleAddIngredient.bind(this);
        this.state = {
            addIngredient: false,
            editedIngredient: {
                ingredient: '',
                amount: 0,
                description: '',
                idRecipeIngredientRelation: ''
            },
            editingExistingIngredient: false
        }
        this.handleRowClick = this.handleRowClick.bind(this)
        this.handleFormChange = this.handleFormChange.bind(this);
        this.handleRowDeselect = this.handleRowDeselect.bind(this);
    }
    handleAddIngredient(e){
        this.setState({
            addIngredient: !this.state.addIngredient
        })
    }
    handleRowClick(ing){
        if (this.state.addIngredient === false)
            return;
        const am = ing.amount === ''? 0 : parseInt(ing.amount);
        this.setState({
            editedIngredient: {
                ingredient: ing.ingredient,
                description: ing.description,
                amount: am,
                idRecipeIngredientRelation: parseInt(ing.idRecipeIngredientRelation)
            },
            editingExistingIngredient: true
        });

    }
    handleFormChange(e) {
        const val = e.target.value;
        var prevIngredient = this.state.editedIngredient;
        prevIngredient[e.target.name] = e.target.value
        this.setState( {editedIngredient: prevIngredient
        });
    }
    handleRowDeselect(e){
        this.setState({
            editedIngredient: {
                ['ingredient']: '',
                ['amount']: 0,
                ['description']: '',
                ['idRecipeIngredientRelation']: ''
            },
            editingExistingIngredient: false
        });
    }

    render(){
        const rows = [];
        this.props.recipeSteps.forEach((rir) =>{
            rows.push(
                <IngredientsRow ingredient={rir} key={rir.idRecipeIngredientRelation} handleRowClick={this.handleRowClick} onRowDeselect={this.handleRowDeselect}/>
            )}
        );
        return(
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>Ingredient</th>
                        <th>amount</th>
                        <th>description</th>
                        <th>delete</th>
                    </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <button onClick={this.handleAddIngredient}>Edit Ingredients</button>
                <IngredientForm recipe={this.props.highlightedRecipe}
                                toggled={this.state.addIngredient}
                                existingIngredient={this.state.editedIngredient}
                                editingExistingIngredient={this.state.editingExistingIngredient}
                                onFormChange={this.handleFormChange}
                                handleRowDeselect={this.handleRowDeselect}
                                addIngredientsRow={this.props.addIngredientsRow}
                />
            </div>

        );
    }
}
/*
Ingredient insertion from
props:
existingIngredient: selected ingredient from ingredient rows, can also be a default value if
an ingredient is deselected. Value's states are changed from the parent class via the onFormChange handler
onFormChange: handler for changes in forms, uses the target.name field to assign the object name and the
target.value field to assign the value.
 */
class IngredientForm extends React.Component{
    constructor(props) {
        super(props);

        this.onFormChange = this.onFormChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRowDeselect = this.onRowDeselect.bind(this);
    }
    onFormChange(e){
        this.props.onFormChange(e);
    }
    handleSubmit(e){
        //create a mutable copy of our state

        e.preventDefault();
        var obj = JSON.parse(JSON.stringify(this.props.existingIngredient));
        obj['amount'] = parseInt(obj['amount']);
        obj['IdRecipeFk'] = this.props.recipe.idRecipe;
        var classPtr = this;
        var submission = new XMLHttpRequest();
        if (this.props.editingExistingIngredient === false) {
            delete obj['idRecipeIngredientRelation'];
            submission.open('POST', DomainLink+Config.PostRecipeIngredientRelation, true);
            submission.setRequestHeader("Content-Type", "application/json");
            const res = JSON.stringify(obj);
            submission.send(res);
            submission.onload = function(){
                var receivedObj = JSON.parse(this.response);
                var newIngredient = classPtr.props.existingIngredient;
                if (receivedObj.status === 404) {
                    alert("error posting ingredient");
                    return;
                }
                newIngredient['idRecipeIngredientRelation'] = receivedObj.idRecipeIngredientRelation;
                classPtr.props.addIngredientsRow(newIngredient);
                console.log(newIngredient);
                classPtr.props.handleRowDeselect(e);
            }

        }
        else {
            submission.open('PUT', DomainLink+Config.PutRecipeIngredientRelation+obj.idRecipeIngredientRelation, true);
            submission.setRequestHeader("Content-Type", "application/json");
            const res = JSON.stringify(obj);
            submission.send(res);
            this.props.handleRowDeselect(e);
        }


    }
    onRowDeselect(e){
        e.preventDefault();
        this.props.handleRowDeselect(e);
    }
    render(){
        let EditingButton;
        if (this.props.editingExistingIngredient===true){

            EditingButton =  <button onClick={this.onRowDeselect}> Deselect Ingredient</button>
        }
        else
            EditingButton= null;

        if (this.props.toggled === true)
            return (
                <form onSubmit={this.handleSubmit}>
                    <label>Ingredient:
                        <input type="text" value={this.props.existingIngredient.ingredient} name="ingredient" onChange={this.onFormChange}/>
                    </label>
                    <label>Amount:
                        <input type="number" value={this.props.existingIngredient.amount} name="amount" onChange={this.onFormChange}/>
                    </label>
                    <p>Description</p>
                    <label>
                        <textarea value={this.props.existingIngredient.description} name="description" onChange={this.onFormChange}/>
                    </label>
                    <button>Submit</button>
                    {EditingButton}
                </form>
            );

        else return(null);
    }
}
export default IngredientsTable;