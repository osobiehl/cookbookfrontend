//creates a table of comments


/*
props:
idRecipe: the recipe id of the highlighted recipe, used for POST and PUT request to attach
the related recipe id of the comment
comments: a json array of comments received from RecipeSearchTable api calls, returns a list of all comments
with the corresponding idRecipe key.


 */
import React from "react";
const Config = require('./config.json');
const DomainLink = Config.host + Config.port + Config.restApiRoot;

class CommentsTable extends React.Component{
    constructor(props) {
        super(props)
        this.defaultComment = {
            idComment: -1,
            firstName: "",
            lastName: "",
            description: "",
            idUser: -1,
            created: "",
            idRecipe: this.props.idRecipe,
        }

        this.state = {
            editingExistingComment: false,
            highlightedComment: this.defaultComment,
            rows: [],
            editingMode: false
        }
        this.handleCommentSelection = this.handleCommentSelection.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.deselectComment = this.deselectComment.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this).bind(this)
        this.verifySubmission = this.verifySubmission.bind(this);
    }

    handleCommentSelection(com){
        this.setState({
            highlightedComment: com,
            editingExistingComment: true
        });
    }

    handleFormChange(e){
        var prevState = this.state.highlightedComment;
        prevState[e.target.name] = e.target.value;
        this.setState({
            highlightedComment: prevState
        });
    }
    deselectComment(e =null){
        if (e != null)
            e.preventDefault();
        this.setState({
            editingExistingComment: false,
            highlightedComment: this.defaultComment

        });
    }

    handleSubmit(e){
        let com = this.state.highlightedComment;
        this.verifySubmission(com);
        com['idRecipe']=this.props.idRecipe;
        let req = new XMLHttpRequest();
        if (this.state.editingExistingRecipe === true){
            req.open('PUT', DomainLink+Config.PutComment)
        }
        else{
            delete com.idComment;
            req.open('POST', DomainLink+Config.PostComment, true);
            com.created = new Date();
        }
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify(com));
        req.onload = function(){
            console.log("sent!");
        }
        this.deselectComment();
    }

    verifySubmission(com){
        com.idComment = parseInt(com.idComment);
        com.idUser = parseInt(com.idUser);
    }


    render() {
        let DeselectButton = null;
        if (this.state.editingExistingComment === true)
            DeselectButton = <button onClick={this.deselectComment}>Deselect comment</button>

        let EditingButton = <button onClick={()=>{this.setState({editingMode: !this.state.editingMode })}}>
            {this.state.editingMode === true ? "remove editing mode" : "editing mode"}
        </button>;

        let highlightComment = null;
        if(this.state.editingMode === true){
            highlightComment= <HighlightComment highlightedComment={this.state.highlightedComment}
                                                handleFormChange={this.handleFormChange}
                                                handleSubmit={this.handleSubmit}></HighlightComment>
        }

        const rows = [];
        this.props.comments.forEach((com) => {
            rows.push( <CommentsRow comment={com} key={com.idComment} handleCommentSelection={this.handleCommentSelection} /> )}
        );
        return (
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>Comment ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>description</th>
                        <th>created on</th>
                        <th>Delete </th>
                    </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                {EditingButton}
                {highlightComment}
                {DeselectButton}
            </div>
        )
    }
}


/*
Handles response related to Comments in the table
props: comment : the comment object that will be rendered
handleCommentSelection : sets the selected comment as the highlightedComment in the parent CommentsRecipe class

 */

class CommentsRow extends React.Component{
    constructor(props){
        super(props);
        this.deleteComment = this.deleteComment.bind(this);
        this.state = {
            deleted: false
        }
    }
    //deletes a comment from the database.
    deleteComment(e){
        e.preventDefault();
        this.setState({
            deleted:true
        });
        let req = new XMLHttpRequest();
        req.open('DELETE', DomainLink+Config.DeleteComment + this.props.comment.idComment);
        req.send();
        console.log("deleting comment . . .");
    }

    render() {
        const com = this.props.comment
        if (this.state.deleted !== true)
            return (
                <tr onClick={() =>{this.props.handleCommentSelection(com)}}>
                    <td>{com.idComment}</td>
                    <td>{com.firstName}</td>
                    <td> {com.lastName}</td>
                    <td> {com.description}</td>
                    <td> {com.created}</td>
                    <td> <button onClick={this.deleteComment}>X </button> </td>
                </tr>
            );
        else return(null);
    }
}

/*
props:
handleSubmit: handles the submission of a comment to the database
highlightedComment: a comment highlighted from the parent CommentTable class,
handleFormChange: handler to change the state highlightedComment in the parent class,
handler uses target.name to identify the field to be changed and target.value for the value
 */

function HighlightComment(props){

    return (
        <form onSubmit={props.handleSubmit}>
            <table>
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <input type="text" name="firstName" value={props.highlightedComment.firstName} onChange={props.handleFormChange} />
                    </td>
                    <td>
                        <input type="text" name="lastName" value={props.highlightedComment.lastName} onChange={props.handleFormChange} />
                    </td>
                    <td>
                        <textarea name="description" value={props.highlightedComment.description} onChange={props.handleFormChange} />
                    </td>
                </tr>
                </tbody>
            </table>
            <button>Submit</button>
        </form>
    )
}
export default CommentsTable;
