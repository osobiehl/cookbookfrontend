import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CommentsTable from "./Comments";
import IngredientsTable from "./Ingredients";
const Config = require('./config.json');
const DomainLink = Config.host + Config.port + Config.restApiRoot

//Searchbar class used for searching recipes
class SearchBar extends React.Component{
    constructor(props){
        super(props);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
    }

    handleSearchTextChange(e){
        this.props.onSearchTextChange(e.target.value);
        this.props.onRowDeselect();
    }
    render(){
        return(
            <form>
                <input className={"innerSearchBar"}
                    type="text"
                    placeholder="Search. . ."
                    value={this.props.searchText}
                    onChange={this.handleSearchTextChange}
                    />
            </form>

        );
    }
}
// "root" class of search bar + recipe table functionality,
/*
    states:
    search text: current text being searched, updated ASYNCHRONOUSLY, use value used in handleSearchTextChange for current values
    recipes: JSON array of recipe objects, updated by handleRecipeChange method
 */


// a row of recipes, contains all of the values returned by a RecipeView object, oraginnzed as a table
class RecipeRow extends React.Component{
    constructor(props) {
        super(props);
        this.onRowClick = this.onRowClick.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.state = {
            deleted: false
        }
    }

    onRowClick(recipe){
        this.props.handleRowClick(recipe);
    }
    onDelete(e){
        e.preventDefault();
        this.setState({
            deleted: true
        });
        let deleteRequest = new XMLHttpRequest();
        deleteRequest.open('DELETE', DomainLink + Config.DeleteRecipe + this.props.recipe.idRecipe, true)
        deleteRequest.send();
        deleteRequest.onload= function (){

        }
    }
    render(){

        const recipe = this.props.recipe;
        if (this.state.deleted === false)
            return (
            <tr onClick ={() => { this.onRowClick(recipe) }} >
                <td>{recipe.idRecipe}</td>
                <td>{recipe.name}</td>
                <td> {recipe.recipeTypeName}</td>
                <td> {recipe.recipeCategoryName}</td>
                <td> {recipe.idSpoons}</td>
                <td> {recipe.chefFirstName}</td>
                <td> {recipe.chefLastName}</td>
                <td> {recipe.preptime}</td>
                <td> {recipe.cooktimehours}</td>
                <td> {recipe.cooktimeminutes}</td>
                <td> {recipe.description}</td>
                <td> {recipe.originName}</td>
                <td> <button onClick={this.onDelete}> X </button></td>
            </tr>
        );
        else return(null);
    }

}

//a table of recipe rows
/*
props:
handleRowClick: a handler for when a row is clicked to highlight it
 */
class RecipeTable extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const rows = [];
        this.props.recipes.forEach((rec) => {
            rows.push(
                <RecipeRow
                recipe={rec}
                key = {rec.idRecipe}
                handleRowClick= {this.props.handleRowClick}/>
            );
        });
        //creates table head rows
        return(
            <table>
                <thead>
                <tr>
                    <th>Recipe ID</th>
                    <th>name</th>
                    <th>type</th>
                    <th>category</th>
                    <th>spoons</th>
                    <th>first name</th>
                    <th>last name</th>
                    <th>preparation time</th>
                    <th> Cooking time (hours) </th>
                    <th> Cooking time (minutes) </th>
                    <th> description</th>
                    <th> Origin </th>
                    <th> Delete </th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
}

//main class: responsible for click event handling
class SearchRecipeTable extends React.Component{
    constructor(props) {
        super(props);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
        this.handleRecipeChange = this.handleRecipeChange.bind(this);
        this.handleRowClick = this.handleRowClick.bind(this);
        this.handleRecipeEdit = this.handleRecipeEdit.bind(this);
        this.setHighlightedRecipeType = this.setHighlightedRecipeType.bind(this);
        this.setHighlightedRecipeCategory = this.setHighlightedRecipeCategory.bind(this);
        this.DeselectRecipe = this.DeselectRecipe.bind(this);
        this.handleRecipeFormChange = this.handleRecipeFormChange.bind(this);
        this.sortFromButton = this.sortFromButton.bind(this);
        this.addRecipe = this.addRecipe.bind(this);
        this.addIngredientsRow = this.addIngredientsRow.bind(this);
        this.defaultRecipe = {calories: null,
            chefFirstName: "",
            chefLastName: "",
            comment: [],
            cooktimehours: 0,
            cooktimeminutes: 0,
            description: "",
            idChef: -1,
            idChefNavigation: null,
            idOrigin: -1,
            idOriginNavigation: null,
            idRecipe: -1,
            idRecipeCategory: -1,
            idRecipeCategoryNavigation: null,
            idRecipeType: -1,
            idRecipeTypeNavigation: null,
            idSpoons: 0,
            name: "",
            originName: "",
            preptime: 0,
            recipeCategoryName: "",
            recipeTypeName: "",
            refrigerationtime: 0
        }
        this.state = {
            searchText: '',
            recipes:  [],
            highlightedRecipe: this.defaultRecipe,
            comment: [],
            recipeSteps: [],
            addIngredient: false,
            recipeEdit: false,
            editingExistingRecipe: false
        };


    }
    // uses api to search for available recipes
    handleSearchTextChange(searchText){
        this.setState({searchText: searchText});
        const get = new XMLHttpRequest();
        if (! searchText)
            return;
        get.open('GET', DomainLink+Config.SearchRecipe+searchText, true);
        get.send();
        // async method to work stuff out
        let class_ptr = this;
        get.onload = function(){
            if (this.response.status === 404)
                return;
            let JSON_obj = JSON.parse(this.response);
            class_ptr.handleRecipeChange(JSON_obj);
        }
    }
    // handles changes when a row is clicked, responsible for:
    // passing a selected row to see all info
    // get the new set of comments related to the recipe id

    handleRowClick(recipe){
        if(this.state.recipeEdit === true){
            this.setState({
                editingExistingRecipe: true
            })
        }
        this.setState({
            highlightedRecipe: recipe
        });

        const cm = new XMLHttpRequest();
        cm.open('GET',  DomainLink + Config.GetCommentsRecipe + recipe.idRecipe, true);
        cm.send();
        const class_ptr = this;
        cm.onload = function(){
            if (this.status === 404)
                return;
            let comment_JSON = JSON.parse(cm.response);
            class_ptr.setState( {comment: comment_JSON});

        }
        //handler for recipeingredient relation
        const RIR_req = new XMLHttpRequest();
        RIR_req.open('GET', DomainLink+Config.GetIngredients + recipe.idRecipe, true);
        RIR_req.send();
        RIR_req.onload = function(){
            if (this.status === 404)
                return;
            let RIR_JSON = JSON.parse(RIR_req.response);
            class_ptr.setState({recipeSteps: RIR_JSON} );
        }



    }

    handleRecipeChange(JSON_obj){
        this.setState(
            {recipes: JSON_obj}
        )

    }

    handleRecipeEdit(e){
        e.preventDefault();
        this.setState({
            recipeEdit: !this.state.recipeEdit,
        });
    }

    setHighlightedRecipeType(e){
        var parsedResult = JSON.parse(e.target.value);
        var temp = this.state.highlightedRecipe
        temp['idRecipeType'] = parsedResult.idRecipeType;
        temp['recipeTypeName'] = parsedResult.name;
        this.setState({
            highlightedRecipe: temp
        });
    }

    setHighlightedRecipeCategory(e){
        var parsedResult = JSON.parse(e.target.value);

        var temp = this.state.highlightedRecipe
        temp['idRecipeCategory']= parsedResult.idRecipeCategory;
        temp['recipeCategoryName']= parsedResult.name;
        this.setState({
            highlightedRecipe: temp
        });
    }

    DeselectRecipe(e=null){
        if (e != null)
            e.preventDefault();
        this.setState({
            editingExistingRecipe: false,
            highlightedRecipe: this.defaultRecipe,
            comment: [],
            recipeSteps: []
        })
    }

    handleRecipeFormChange(e){
        var lastres= this.state.highlightedRecipe;
        lastres[e.target.name] = e.target.value;
        this.setState({
            highlightedRecipe: lastres
        })
    }

    sortFromButton(e){
        e.preventDefault();
        let req = new XMLHttpRequest();
        req.open('GET', DomainLink + e.target.name);
        req.send();
        let classPtr = this;
        req.onload = function(){
            classPtr.setState({
                recipes: JSON.parse(this.response)
            })
        }
    }

    addRecipe(newRecipe){
        let newRecipes = this.state.recipes.concat(newRecipe);
        this.setState({
            recipes: newRecipes
        })
    }
    addIngredientsRow(newIngredient){
        let newIngredients = this.state.recipeSteps.concat(newIngredient);
        this.setState({
            recipeSteps: newIngredients
        });

    }

    render(){
        let DeselectButton;
        if (this.state.editingExistingRecipe === true || this.state.highlightedRecipe.idRecipe !== -1)
            DeselectButton = <button onClick={this.DeselectRecipe}>Deselect Recipe</button>;
        else DeselectButton = null;
        return(
            <div>
                <button name={Config.GetRecipeByCategory} onClick={this.sortFromButton}>Get Recipes by Category</button>
                <button name={Config.GetRecipeByType} onClick={this.sortFromButton}>Get Recipes by Type</button>
                <button name={Config.GetMajorRankByCategory} onClick={this.sortFromButton}> Get Major Rank by Category </button>
                <SearchBar
                    searchText = {this.state.searchText}
                    onSearchTextChange = {this.handleSearchTextChange}
                    onRowDeselect={this.DeselectRecipe}
                />

                <RecipeTable
                    recipes={this.state.recipes}
                    handleRowClick = {this.handleRowClick}
                />
                <div>
                    <button onClick={this.handleRecipeEdit}> editing mode </button>
                </div>
                <HighLightRecipe highlightedRecipe = {this.state.highlightedRecipe}
                                 editingMode={this.state.recipeEdit}
                                 setHighlightedRecipeCategory={this.setHighlightedRecipeCategory}
                                 setHighlightedRecipeType={this.setHighlightedRecipeType}
                                 handleRecipeFormChange={this.handleRecipeFormChange}
                                 editingExistingRecipe={this.state.editingExistingRecipe}
                                 addRecipe={this.addRecipe}
                />
                {DeselectButton}
                <IngredientsTable recipeSteps={this.state.recipeSteps} highlightedRecipe={this.state.highlightedRecipe} addIngredientsRow={this.addIngredientsRow} />
                <CommentsTable comments={this.state.comment} idRecipe={this.state.highlightedRecipe.idRecipe}/>
            </div>
        );
    }
}
/*
props:
-Types: an object array containing all type names and id's gotten from the API on page startup, these values are not re-calculated
Function responsible for calling the api to get a list of all types of recipes, is called only on page startup, does not refresh
  IMPORTANT: The option return value is a stringified tuple containing both the type id and type name,
    it must be parsed by the handler for form submission
 */
function RecipeTypes(props){
    const options = [];
    options.push(<option key={-1} value={JSON.stringify({name: 'default', idRecipeType: -1})}> select a type . . .</option>)
    props.Types.forEach((typ) => {
        options.push(
            <option value={JSON.stringify({name: typ.name,
            idRecipeType: typ.idRecipeType})}
                    key={typ.idRecipeType}  name={typ.name}> {typ.name} </option>
        );
    });
    return(options);
}
/*
returns a list of categories associated with a type, is received from the API on recipeType change.
props:
-Categories: a list of categories returned by the API based on the currently selected recipe type
    IMPORTANT: The option return value is a stringified tuple containing both the id and category name,
    it must be parsed by the handler for form submission
 */
function RecipeCategories(props){
    const options = [];
    options.push(<option value={{ "idRecipeCategory": "-1", "name": "select a category. . . "}}
    key={-1}
    >Select Category...</option>);

    props.Categories.forEach((cat)=>
    options.push(
        <option  value={ JSON.stringify({idRecipeCategory: cat.idRecipeCategory,
            name:cat.name})}
                key={cat.idRecipeCategory}
                name={cat.name}>{cat.name}</option>
    ));
    return(options);
}
/*
    class responsible for  highlighting recipes for editing
    props:
    -handleRecipeFormChange: a function that updates the state of the SearchRecipeTable class,
    it changes the state of the highlightedRecipe class to match the new input put in by the user
    -highlightedRecipe: the highlighted recipe that is selected to be changed
    editingExistingRecipe: condition that decides whether a PUT or a POST will
    be executed when the form is submitted.
    -editingMode: a condition to decide if this component will render: renders on true
    -onFormChange: a handler to change the state of fields specified in the form, uses e.target.name for the
    name of the field to be assigned and e.target.value for the value.
    -addRecipe: handler to add a recipe to the recipes[] object in the parent class after a successful post


 */
class HighLightRecipe extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Types: [],
            SelectedType: null,
            Categories: [],
            idSpoons: ''
        }

        let RecipeCategory = new XMLHttpRequest()
        RecipeCategory.open('GET', DomainLink + Config.GetType, true);
        RecipeCategory.send();
        var classPtr = this;
        RecipeCategory.onload = function(){
            let jsonObj = JSON.parse(this.response);
            classPtr.setState({
                Types: jsonObj
            });
        }
        //binding for methods
        this.handleTypeChange= this.handleTypeChange.bind(this);
        this.handleCategoryChange=this.handleCategoryChange.bind(this);
        this.onFormChange = this.onFormChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    //calls parent class handler to change state of values.
    onFormChange(e){
        this.props.handleRecipeFormChange(e)
    }

    //handles the change of a recipe type selection change, loads in all categories
    //associated to the recipe change
    handleTypeChange(e){
        //corner case: select is selected
        var parsedEvent = JSON.parse(e.target.value);

        if(parsedEvent.idRecipeType  === -1)
            return;
        let cat_req = new XMLHttpRequest();
        cat_req.open('GET', DomainLink+Config.GetCategoryFromType+parsedEvent.idRecipeType, true);
        cat_req.send();
        var classPtr = this;
        cat_req.onload = function(){
            var response = JSON.parse(this.response);
            classPtr.setState({
                Categories: response
            });
        }
        this.props.setHighlightedRecipeType(e);
    }
    //handles the change of user selection of a category

    handleCategoryChange(e){
        this.props.setHighlightedRecipeCategory(e);
    }

    // handles submission of a recipe form
    onSubmit(e){
        e.preventDefault()
        var recipe = this.props.highlightedRecipe;


        if (this.verifyRecipe(recipe)) {
            var req = new XMLHttpRequest();

            if (this.props.editingExistingRecipe === true) {
                req.open('PUT', DomainLink + Config.PutRecipe + JSON.stringify(recipe.idRecipe), true);
                req.send(JSON.stringify(recipe));
                req.onload=function(){
                }
            }
            //else we do a post
            else{
                req.open('POST', DomainLink+ Config.PostRecipe);
                delete recipe.idRecipe;
                var classPtr = this;
                req.setRequestHeader("Content-Type", "application/json");
                req.send(JSON.stringify(recipe));
                req.onload=function(){
                    let response = JSON.parse(this.response);
                    if (response.status === 400) {
                        alert("an error occurred");
                        return;
                    }
                    classPtr.props.addRecipe(response);

                }
            }

            }
        else {
            alert("could not verify that recipe fields are correctly filled");
        }

    }
    // get rid of all the nasty stringification JS can sometimes do
    // MUTABLE METHOD
    verifyRecipe(recipe){
        recipe.idRecipeCategory = parseInt(recipe.idRecipeCategory);
        recipe.idChef = parseInt(recipe.idChef);
        recipe.idOrigin = parseInt(recipe.idOrigin);
        recipe.idRecipeType = parseInt(recipe.idRecipeType);
        recipe.preptime = parseInt(recipe.preptime);
        recipe.cooktimehours = parseInt(recipe.cooktimehours);
        recipe.refrigerationtime = parseInt(recipe.refrigerationtime);
        recipe.cooktimeminutes = parseInt(recipe.cooktimeminutes);
        recipe.idSpoons = parseInt(recipe.idSpoons);
        console.log(recipe);
        if (recipe.idRecipeCategory === -1)
            return false;
        else if (recipe.idRecipeType === -1)
            return false
        else return true;

    }
    render() {
        if (this.props.editingMode === false) {
            return (null);
        }
        else return (
            <div>
                <form onSubmit={this.onSubmit} /*add onsubmit*/>
                    <table>
                        <thead>
                        <tr>
                            <th >name</th>
                            <th>type</th>
                            <th>category</th>
                            <th>spoons</th>
                            <th>first name</th>
                            <th>last name</th>
                            <th>preparation time</th>
                            <th> Cooking time (hours) </th>
                            <th> Cooking time (minutes) </th>
                            <th> description</th>
                            <th> Origin </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <label>Name <input type="text" value={this.props.highlightedRecipe.name} name="name" onChange={this.onFormChange}/>
                                </label>
                            </td>
                            <td>
                                <select onChange={this.handleTypeChange} name="Type">
                                    <RecipeTypes Types={this.state.Types}/>
                                </select>
                            </td>
                            <td>
                                <select onChange={this.handleCategoryChange} >
                                    <RecipeCategories Categories={this.state.Categories} name="category"  />
                                </select>
                            </td>
                            <td>
                                <select value={this.props.highlightedRecipe.idSpoons} name={"idSpoons"} onChange={this.onFormChange}>
                                    <option value={1}>1</option>
                                    <option value={2}>2</option>
                                    <option value={3}>3</option>
                                    <option value={4}>4</option>
                                    <option value={5}>5</option>
                                </select>

                            </td>
                            <td>
                                <input type="text" value={this.props.highlightedRecipe.chefFirstName} name={"chefFirstName"} onChange={this.onFormChange}/>
                            </td>
                            <td>
                                <input type="text" value={this.props.highlightedRecipe.chefLastName} name={"chefLastName"} onChange={this.onFormChange}/>
                            </td>
                            <td>
                                <input type="number" value={this.props.highlightedRecipe.preptime} name={"preptime"} onChange={this.onFormChange}/>
                            </td>
                            <td>
                                <input type="number" value={this.props.highlightedRecipe.cooktimehours} name={"cooktimehours"} onChange={this.onFormChange}/>
                            </td>
                            <td>
                                <input type="number" value={this.props.highlightedRecipe.cooktimeminutes} name={"cooktimeminutes"} onChange={this.onFormChange}/>
                            </td>
                            <td>
                                <textarea value={this.props.highlightedRecipe.description} name="description" onChange={this.onFormChange}/>
                            </td>
                            <td>
                                <input type="text" value={this.props.highlightedRecipe.originName} name="originName" onChange={this.onFormChange}/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <button>Submit</button>
                </form>
            </div>
        );

    }
}

function foo(){
    console.log("bar");
}





ReactDOM.render(
    <SearchRecipeTable />,
    document.getElementById('root'),
);


